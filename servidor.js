const openid = require('openid');
const url = require('url');
const querystring = require('querystring');
const express = require("express");
const app = express();
const session = require("express-session");
const bodyParser = require("body-parser");
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const http = require("http").Server(app);
const io =require("socket.io")(http);
const Promise = require("bluebird");
const randomNumber = require("random-number-csprng");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static("public"));

//-----------------------------Ajustes de session-------------------------------
const sessionsettings = {
  secret: 'Jas712jasK',
  name: "Gamerkit",
  resave: true,
  saveUninitialized: true,
  maxAge :  2 * 60 * 60 * 1000,
  cookie: {}
};

app.use(session(sessionsettings));

//---------------------Ajustes inicio de sesion steam---------------------------
const relyingParty = new openid.RelyingParty(
    'http://localhost:3000/respuestasteam', // Verification URL (yours)
    null, // Realm (optional, specifies realm for OpenID authentication)
    true, // Use stateless verification
    false, // Strict mode
[]); // List of extensions to enable and include


//-----------------------------Pagina principal---------------------------------
app.get("/", function(req,res){
   if(req.session.connected){
      res.sendFile(__dirname + "/public/principal.html");
   } else {
      res.redirect("login");
   }
})

//-------------------------------Login -----------------------------------------
app.get("/login", function(req,res){
   if(req.session.connected){
      res.redirect("/");
   } else {
      res.sendFile(__dirname + "/public/login.html");
   }
});

//--------------------Redirecciona a steam para login---------------------------
app.get("/steamlogin",function(req,res){
      var identifier = "http://steamcommunity.com/openid";
      relyingParty.authenticate(identifier, false, function(error, authUrl){
           if (error) {
                res.writeHead(200);
                res.end('Authentication failed: ' + error.message);
           } else if (!authUrl) {
                res.writeHead(200);
                res.end('Authentication failed');
           } else {
                res.writeHead(302, { Location: authUrl });
                res.end();
           }
      });
});

//------------------Recivir la conexion de steam + activar session--------------
app.get("/respuestasteam",function(req,res){
   relyingParty.verifyAssertion(req, function(error, result) {
      var validOpEndpoint = 'https://steamcommunity.com/openid/login';
      var identifierRegex = /^http:\/\/steamcommunity\.com\/openid\/id\/(\d+)$/;
      var steamId = identifierRegex.exec(result.claimedIdentifier)[1];
      req.session.connected = true;
      req.session.steamId = steamId;
      //console.log(req.session.steamId);
      res.redirect("/");
   })
});
app.get("/userlogout",function(req,res){
   req.session.destroy();

});
app.get("/userinfo",function(req,res){
   var xmlhttp = new XMLHttpRequest();
   var url = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0001?key=B549B6ABC62E3A2C0D6002D39D347039&steamids=" + req.session.steamId;
   xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        usersteaminfo = JSON.parse(this.responseText);
        res.send(JSON.stringify({personaname: usersteaminfo.response.players.player[0].personaname,avatar: usersteaminfo.response.players.player[0].avatar}));
       // console.log(usersteaminfo.response.players.player[0].personaname);
      };
   };
   xmlhttp.open("GET", url, true);
   xmlhttp.send();
})

var usersonline = 0;
setInterval(actualizarusersonline,10*1000);

function actualizarusersonline(){
   io.emit("actualizarusersonline",usersonline);
}
io.on('connection', function(socket){
   usersonline++;
   socket.on('disconnect', function(){
      usersonline--;
   });
  socket.on('enviarmensaje', function(msg){
    if (msg[2] !== ""){
      io.emit('broadcastmensaje', msg);
    }
  });
});


http.listen(3000,function(){
   console.log("<<<<<<<<<<<<---Serv working on 3000--->>>>>>>>>>>>>");

});
